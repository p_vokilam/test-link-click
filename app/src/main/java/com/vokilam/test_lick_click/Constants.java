package com.vokilam.test_lick_click;

/**
 * Created by vokilam on 9/6/16.
 */
public class Constants {
    public static final class Urls {
        public static final String TERMS_EN = "http://viber.com/en/terms";
        public static final String TERMS_RU = "http://viber.com/ru/terms";
        public static final String TERMS_JP = "http://viber.com/jp/terms";
        public static final String PRIVACY = "http://viber.com/privacy";
    }
}
