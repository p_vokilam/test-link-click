package com.vokilam.test_lick_click;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by vokilam on 9/6/16.
 */
public class DeepLinkActivity extends AppCompatActivity {

    private DeepLinkResolver deepLinkResolver;
    private Navigator navigator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        deepLinkResolver = new DeepLinkResolver(InternalDeepLinks.values());
        navigator = new Navigator(this);

        Intent intent = getIntent();
        Uri uri = intent.getData();

        if (!Intent.ACTION_VIEW.equals(intent.getAction()) || uri == null) {
            finish();
            return;
        }

        // Prevent from restoring by system
        // We don't want to handle the same data twice
        intent.setDataAndType(null, intent.getType());
        setIntent(intent);

        resolveUri(uri);
    }

    private void resolveUri(Uri uri) {
        deepLinkResolver.resolve(this, uri, navigator);
        finish();
    }
}
