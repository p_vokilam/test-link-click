package com.vokilam.test_lick_click;

import android.content.Context;
import android.net.Uri;

/**
 * Created by vokilam on 9/6/16.
 */
public enum InternalDeepLinks implements DeepLink {
    TERMS_OF_USE("terms_of_use") {
        @Override
        public void resolve(Context context, Uri uri, Navigator navigator) {
            navigator.openTermsAndConditions();
        }
    },
    PRIVACY_POLICY("privacy_policy") {
        @Override
        public void resolve(Context context, Uri uri, Navigator navigator) {
            navigator.openPrivacyPolicy();
        }
    };

    private static final String INTERNAL = "internal";
    private final String path;

    InternalDeepLinks(String path) {
        this.path = path;
    }

    @Override
    public String getAuthority() {
        return INTERNAL;
    }

    @Override
    public String getPath() {
        return path;
    }
}
