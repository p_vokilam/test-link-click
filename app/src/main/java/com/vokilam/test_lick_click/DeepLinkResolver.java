package com.vokilam.test_lick_click;

import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by vokilam on 9/6/16.
 */
public final class DeepLinkResolver {

    private UriMatcher mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private List<DeepLink> mUrlSchemes = new ArrayList<>();

    public DeepLinkResolver(DeepLink[]... groups) {
        for (DeepLink[] group : groups) {
            Collections.addAll(mUrlSchemes, group);
        }

        int matchId = 0;
        for (DeepLink scheme : mUrlSchemes) {
            mUriMatcher.addURI(scheme.getAuthority(), scheme.getPath(), matchId++);
        }

        if (BuildConfig.DEBUG) {
            validateDuplicates();
        }
    }

    private void validateDuplicates() {
        Set<String> urls = new HashSet<>();

        for (DeepLink scheme : mUrlSchemes) {
            if (!urls.add(scheme.getAuthority() + scheme.getPath())) {
                throw new IllegalStateException("Deep links must be unique: " + scheme);
            }
        }
    }

    public void resolve(Context context, Uri uri, Navigator navigator) {
        int match = mUriMatcher.match(uri);

        if (match == UriMatcher.NO_MATCH || match >= mUrlSchemes.size()) {
            return;
        }

        mUrlSchemes.get(match).resolve(context, uri, navigator);
    }
}
