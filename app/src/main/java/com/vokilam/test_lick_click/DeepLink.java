package com.vokilam.test_lick_click;

import android.content.Context;
import android.net.Uri;

/**
 * Created by vokilam on 9/6/16.
 */
public interface DeepLink {
    void resolve(Context context, Uri uri, Navigator navigator);
    String getAuthority();
    String getPath();
}
