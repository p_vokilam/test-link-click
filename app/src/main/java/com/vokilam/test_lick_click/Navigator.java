package com.vokilam.test_lick_click;

import android.content.Context;
import android.widget.Toast;

import java.util.Locale;

/**
 * Created by vokilam on 9/6/16.
 */
public class Navigator {

    private final Context context;

    public Navigator(Context context) {
        this.context = context;
    }

    public void openUrl(String url) {
        // TODO launch in-app browser activity
        Toast.makeText(context, url, Toast.LENGTH_LONG).show();
    }

    public void openTermsAndConditions() {
        // draft code
        final String locale = Locale.getDefault().getLanguage().toLowerCase(Locale.US);
        String url;

        if (locale.startsWith("ja")) {
            url = Constants.Urls.TERMS_JP;
        } else if (locale.startsWith("ru")) {
            url = Constants.Urls.TERMS_RU;
        } else {
            url = Constants.Urls.TERMS_EN;
        }

        openUrl(url);
    }

    public void openPrivacyPolicy() {
        openUrl(Constants.Urls.PRIVACY);
    }
}
